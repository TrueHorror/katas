import {howUnlucky} from './index'

describe('Given year should return number o days that is friday the 13th', function() {

    test('2020 should return 2', () => {
        const year = 2020
        const expected = 2
        expect(howUnlucky(year)).toBe(expected)
    })

    test('2026 should return 3', () => {
        const year = 2026
        const expected = 3
        expect(howUnlucky(year)).toBe(expected)
    })

    test('2016 should return 1', () => {
        const year = 2016
        const expected = 1
        expect(howUnlucky(year)).toBe(expected)
    })

    
})