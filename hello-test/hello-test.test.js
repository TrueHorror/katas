import {sortWord} from './index'

test('Sorted words should return letters alphabetically', function() {
    const word = 'dcba'
    expect(sortWord(word)).toBe('abcd')
})
