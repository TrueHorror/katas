export function getLength(nestedArray) {
  if (nestedArray.length == 0) {
    return 0;
  }

  return countItems(nestedArray);
}

function countItems(array) {
  let count = 0;
  for (let i = 0; i < array.length; i++) {
    if (Array.isArray(array[i])) {
      count += countItems(array[i]);
    } else {
      count++;
    }
  }
  return count;
}
