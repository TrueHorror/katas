import {getLength} from './index'

describe('Tests that returns the total number of non-nested items in a nested array', function(){

    test('[1, [2, 3]] should return 3 ', () => {
        
        const nestedArray = [1, [2, 3]]
        const result = 3

        expect(getLength(nestedArray)).toBe(result)
    })

    test('[1, [2, [3, 4]]] should return 4 ', () => {
        
        const nestedArray = [1, [2, [3, 4]]]
        const result = 4

        expect(getLength(nestedArray)).toBe(result)
    })

    test('[1, [2, [3, [4, [5, 6]]]]] should return 6 ', () => {
        
        const nestedArray = [1, [2, [3, [4, [5, 6]]]]]
        const result = 6

        expect(getLength(nestedArray)).toBe(result)
    })

    test('[1, [2], 1, [2], 1] should return 5 ', () => {
        
        const nestedArray = [1, [2], 1, [2], 1]
        const result = 5

        expect(getLength(nestedArray)).toBe(result)
    })

    test('[] should return 0 ', () => {
        
        const nestedArray = []
        const result = 0

        expect(getLength(nestedArray)).toBe(result)
    })
    

})