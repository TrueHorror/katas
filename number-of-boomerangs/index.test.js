import {countBoomerangs} from "./index"

describe("Count number of boomerangs in an array where the third position from x matches x", function(){
    test('[9, 5, 9, 5, 1, 1, 1] should return 2 ', () => {
        expect(countBoomerangs([9, 5, 9, 5, 1, 1, 1])).toBe(2)
    })

    test('[5, 6, 6, 7, 6, 3, 9] should return 1 ', () => {
        expect(countBoomerangs([5, 6, 6, 7, 6, 3, 9])).toBe(1)
    })

    test('[4, 4, 4, 9, 9, 9, 9] should return 0 ', () => {
        expect(countBoomerangs([4, 4, 4, 9, 9, 9, 9])).toBe(0)
    })
    
    test('[1, 7, 1, 7, 1, 7, 1] should return 5 ', () => {
        expect(countBoomerangs([1, 7, 1, 7, 1, 7, 1])).toBe(5)
    })
    
})