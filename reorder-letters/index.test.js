import {reorder} from './index'

describe('Move capital letters to the front and numbers to the back', function() {
    test('"hA2p4Py" should return "APhpy24"', () => {
        const inString = 'hA2p4Py'
        const outString = 'APhpy24'
        expect(reorder(inString)).toBe(outString)
    })

    test('"m11oveMENT" should return "MENTmove11"', () => {
        const inString = 'm11oveMENT'
        const outString = 'MENTmove11'
        expect(reorder(inString)).toBe(outString)
    })

    test('"s9hOrt4CAKE" should return "OCAKEshrt94"', () => {
        const inString = 's9hOrt4CAKE'
        const outString = 'OCAKEshrt94'
        expect(reorder(inString)).toBe(outString)
    })
    
})