export function reorder(string){
    let splitted = string.split('')
    let upperCaseSorted = sortByUppercase(splitted)
    let result = upperCaseSorted.join('').toString()
    console.log(result);
}

function sortByUppercase(array){
   return array.sort(function(a, b){
        if (upperCase(a) && !upperCase(b)) {
            return -1
        } 
        else if (upperCase(b) && !upperCase(a)) {
            return 1
        }
        return a.localeCompare(b)
    })
}

function upperCase(string){
    return string.match(/[A-Z]/)
}

function number(int){
    return int.match(/[0-9]/)
}