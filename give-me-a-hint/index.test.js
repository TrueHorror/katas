import {grantTheHint} from './index'

describe('Reveal first letter of words in given sentences every iteration', function(){
    test(`"Mary Queen of Scots" should return array with:
        "____ _____ __ _____",
        "M___ Q____ o_ S____",
        "Ma__ Qu___ of Sc___",
        "Mar_ Que__ of Sco__",
        "Mary Quee_ of Scot_",
        "Mary Queen of Scots" 
    `, () => {
      const sentence = "Mary Queen of Scots"
      expect(grantTheHint(sentence)).toBe(
        ["____ _____ __ _____",
        "M___ Q____ o_ S____",
        "Ma__ Qu___ of Sc___",
        "Mar_ Que__ of Sco__",
        "Mary Quee_ of Scot_",
        "Mary Queen of Scots"] 
      )
    })
    
})