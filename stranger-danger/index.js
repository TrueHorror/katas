 export function noStrangers(string){
    let aquaintance = []
    let friendlyWordsInOrder = []

    let splittedWords = string.replace(/\./g,'').toLowerCase().split(" ")

    let mappedWords = new Map(splittedWords.map( w => [w,0]))

    splittedWords.forEach(w => {
        mappedWords.set(w, (mappedWords.get(w) + 1))
    })

    mappedWords.forEach((value, key) => {
        if (value == 3 || value == 4) {
            aquaintance.push(key)
        }
        if (value > 4) {
            friendlyWordsInOrder.push(key)
        }
    })



    return [aquaintance, friendlyWordsInOrder]
}