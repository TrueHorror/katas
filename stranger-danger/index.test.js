import {noStrangers} from './index.js'



test('See Spot run. See Spot jump. Spot likes jumping. See Spot fly. should return [["spot", "see"][]]', function(){
    const string = "See Spot run. See Spot jump. Spot likes jumping. See Spot fly."
    expect(JSON.stringify(noStrangers(string))).toBe(JSON.stringify([["see", "spot"],[]])) 
})


