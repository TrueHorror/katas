import {palSeq} from './index'

describe('Return the smallest seed integer of a given palindrome', function () {
    test('Palindrome 1 should return [1, 0]', () => {
        const pal = 1
        const res = [1, 0]
        expect(palSeq(pal)).toEqual(res)
    })

    test('Palindrome 4884 should return [69, 4]', () => {
        const pal = 4884
        const res = [69, 4]
        expect(palSeq(pal)).toEqual(res)
    })
    
    test('Palindrome 9999 should return ', () => {
        const pal = 9999
        const res = [69, 4]
        expect(palSeq(pal)).toEqual(res)
    })

    
})