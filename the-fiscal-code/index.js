const CONSONANTS = {     
        'A': 0, 'B': 1, 'C': 1, 'D': 1, 'E':0, 'F': 1, 'G': 1, 'H': 1, 'I':0,
        'J': 1, 'K':1, 'L':1, 'M':1, 'N':1, 'O': 0, 'P':1,
        'Q':1, 'R':1, 'S':1, 'T':1, 'U': 0, 'V':1, 'W':1,
        'X':1, 'Z':1, 'Y':1
        }

const MONTHS = { 1: "A", 2: "B", 3: "C", 4: "D", 5: "E", 6: "H",7: "L", 8: "M", 9: "P", 10: "R", 11: "S", 12: "T" }

export function fiscalCode(person){
    let fCode = ''
    let surnameLetters = getCapitalLettersFromSurname(person.surname)
    let nameLetters = getCapitalLettersFromName(person.name)
    let dobAndGenderString = getStringFromDateOfBirthAndGender(person.dob, person.gender)
    fcode = fcode + surnameLetters + nameLetters + dobAndGenderString


    return fCode
}

export function getCapitalLettersFromSurname(surname){
    let surnameSplitted = surname.toUpperCase().split('')
    let consonantsOfSurname = []
    let vowelsOfSurname = []
    let surnameWith3Letters = ''

    if (surnameSplitted.length < 3) {
        for (const c of surnameSplitted) {
            if (CONSONANTS[c]) { 
                consonantsOfSurname.push(c) 
            }
            consonantsOfSurname.push('x') // add x to end of string
        }
            
    }
    else{
        for(let c of surnameSplitted){
                if (CONSONANTS[c]) {  
                    consonantsOfSurname.push(c)
                }
                else {
                    vowelsOfSurname.push(c)
                }   
        }

        surnameWith3Letters = consonantsOfSurname.slice(0, 3).join('')

        if (consonantsOfSurname.length = 2) {
            consonantsOfSurname.push(vowelsOfSurname[0]) // add first vowel to the consonants
        }
        
    }

    return surnameWith3Letters
}

function getCapitalLettersFromName(name){
    
    return name.toUpperCase()

}

function getStringFromDateOfBirthAndGender(dob, gender){
    let splittedDob = dob.split('/')
    


    return string.toUpperCase()
}