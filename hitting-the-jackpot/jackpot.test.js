import {testJackpot} from './index'

describe('Slot machine tests', function() {
    test('["@", "@", "@", "@"] should return true', function() {

        const array = ["@", "@", "@", "@"]
        expect(testJackpot(array)).toBe(true)
    
    })
    test('["abc", "abc", "abc", "abc"] should return true', function() {

        const array = ["abc", "abc", "abc", "abc"]
        expect(testJackpot(array)).toBe(true)
            
    })
    test('["SS", "SS", "SS", "SS"] should return true', function() {

        const array = ["SS", "SS", "SS", "SS"]
        expect(testJackpot(array)).toBe(true)
        
    })
    test('["&&", "&", "&&&", "&&&&"] should return false', function() {
        const array = ["&&", "&", "&&&", "&&&&"]
        expect(testJackpot(array)).toBe(false)
    })
    test('["SS", "SS", "SS", "Ss"] should return false', function() {
        const array = ["&&", "&", "&&&", "&&&&"]
        expect(testJackpot(array)).toBe(false)
    })
    
})

