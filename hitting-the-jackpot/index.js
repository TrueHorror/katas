export function testJackpot(array){
    let temp = array[0];
    let allEquals = false;
    
    array.forEach(element => {
        if (element !== temp) {
            allEquals = false
        }else{
            allEquals = true
        }
        
    });

    return allEquals;
}