import {isValidIP} from './index'

describe('Tests if given string is a valid IP format', function(){
    test('"1.2.3.4" should return true', () => {
        const testIP = '1.2.3.4';
        expect(isValidIP(testIP)).toBe(true)
    })

    test('"1.2.3" should return false', () => {
        const testIP = '1.2.3';
        expect(isValidIP(testIP)).toBe(false)
    })

    test('"1.2.3.4.5" should return false', () => {
        const testIP = '1.2.3.4.5';
        expect(isValidIP(testIP)).toBe(false)
    })

    test('"123.45.67.89" should return true', () => {
        const testIP = '123.45.67.89';
        expect(isValidIP(testIP)).toBe(true)
    })

    test('"123.456.78.90" should return false', () => {
        const testIP = '123.456.78.90';
        expect(isValidIP(testIP)).toBe(false)
    })

    test('"123.045.067.089" should return false', () => {
        const testIP = '123.045.067.089';
        expect(isValidIP(testIP)).toBe(false)
    })

    test('"Hello" should return false as it is not an ipv4 ', () => {
        const string = "Hello"
        expect(isValidIP(string)).toBe(false)
    })

    test('"12e.2fs.f.s" should return false as it is not an ipv4 ', () => {
        const string = "12e.2fs.f.s"
        expect(isValidIP(string)).toBe(false)
    })
    
    
})